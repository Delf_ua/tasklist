$(document).ready(function () {
    TaskList.init();
});

let TaskList = {
    'init': function () {
        $('.btnAddTask').on('click', function () {

            TaskList.show_form(0);
        });
        $('.btnEditTask').on('click', function (e) {

            TaskList.show_form($(e.currentTarget).data('item_id'));
        });
        $('.btnLogin').on('click', function (e) {

            TaskList.show_login_form();
        });
        $('.btnLogout').on('click', function (e) {

            TaskList.logout();
        });
    },
    'show_form': function (id) {
        $.ajax(
            {

                type: "POST",
                dataType: "json",
                url: APP_ROOT + '?action=Task.GetForm',
                data: {'task_id': id},
                success: function (response) {
                    let dlgForm = bootbox.dialog({
                        message: response.form_html,
                        closeButton: true,
                        onEscape: function () {
                        },
                        buttons: {
                            btnCancel: {
                                label: "Cancel",
                                className: 'btn-danger',
                                callback: function () {
                                    dlgForm.modal('hide');
                                }
                            },

                            btnOk: {
                                label: "Ok",
                                className: 'btn-info',
                                callback: function () {
                                    $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: APP_ROOT + '?action=Task.SaveForm',
                                        data: {form_data: $('.frmTask').serialize()},
                                        success: function (response) {
                                            if (response.error_msg === '') {
                                                if (id === 0) {
                                                    bootbox.alert('New task has been added',function(){location.href = APP_ROOT;});

                                                } else {
                                                    location.reload();
                                                }
                                                dlgForm.modal('hide');
                                            } else {
                                                $('.frmTask .msgError').html(response.error_msg);

                                            }
                                        }

                                    });
                                    return false;
                                }
                            }
                        }
                    });
                }
            }
        );
    },
    'logout': function () {
        $.ajax(
            {

                type: "POST",
                dataType: "json",
                url: APP_ROOT + '?action=User.Logout',
                data: {},
                success: function (response) {
                    location.reload();
                }
            });
    },
    'show_login_form': function () {
        $.ajax(
            {

                type: "POST",
                dataType: "json",
                url: APP_ROOT + '?action=User.GetLoginForm',
                data: {},
                success: function (response) {
                    let dlgForm = bootbox.dialog({
                        message: response.form_html,
                        onEscape: function () {
                        },
                        buttons: {
                            Cancel: {
                                label: "Cancel",
                                className: 'btn-danger',
                                callback: function () {
                                    dlgForm.modal('hide');
                                }
                            },
                            ok: {
                                label: 'Login',
                                className: 'btn-success',
                                callback: function () {
                                    $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: APP_ROOT + '?action=User.Login',
                                        data: {form_data: $('.frmLogin').serialize()},
                                        success: function (response) {
                                            if (response.success) {

                                                location.reload();

                                                dlgForm.modal('hide');
                                            } else {
                                                $('.frmLogin .msgError').html('Login error');

                                            }
                                        }

                                    });
                                    return false;
                                }
                            }
                        }
                    });
                }
            });


    }

}