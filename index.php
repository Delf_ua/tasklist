<?php
require_once 'config.php';
require_once 'app/init.php';
if(isset($_GET['action'])){
    $ACTION_BOX=explode('.',$_GET['action']);
}else{
    $ACTION_BOX=array('Task','DisplayList');
}
if(count($ACTION_BOX)!==2){
    tasklist\helper\Tool::Get404Error();
}
if(class_exists('tasklist\\controller\\'.$ACTION_BOX[0])){
    $controller_class='tasklist\\controller\\'.$ACTION_BOX[0];
    $controller= new $controller_class();
    $action_name='Action_'.$ACTION_BOX[1];
    if(method_exists($controller,$action_name)) {
        $act_res=$controller->$action_name();
        if(is_array($act_res)){ //return ajax reply
            echo json_encode($act_res);

        }else{
            echo $act_res;
        }
        $controller->AfterActions();
    }else{
        tasklist\helper\Tool::Get404Error();
    }
}else{
    tasklist\helper\Tool::Get404Error();
}



