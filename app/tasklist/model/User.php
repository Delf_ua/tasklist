<?php

namespace tasklist\model;

use tasklist\helper\DB as DB;
use tasklist\helper\Registry as Registry;

class User
{
    /**
     * @var DB $DB
     */
    private $DB;

    public function __construct()
    {
        $this->DB = Registry::get('DB');
    }


    public function Login($login, $pass)
    {
        $sql = 'select * from user u where u.user_login=LOWER("' . $this->DB->escape($login) . '") and u.user_pass="' . md5($pass) . '" and user_group=1';

        $item = $this->DB->uniSQL($sql);
        if (count($item) == 1) {
            $item = $item[0];
        } else {
            $item = false;
        }
        return $item;
    }

}