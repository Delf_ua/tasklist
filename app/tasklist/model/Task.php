<?php

namespace tasklist\model;

use tasklist\helper\DB as DB;
use tasklist\helper\Registry as Registry;

class Task
{
    /**
     * @var DB $DB
     */
    private $DB;

    public function __construct()
    {
        $this->DB = Registry::get('DB');
    }

    public function GetItems($start, $limit, $sort_by = '', $sort_direction = '')
    {

        if (!in_array($sort_by, array('date_added', 'user_name', 'email', 'status_name'))) {
            $sort_by = 't.date_added';
        }

        if (!in_array(strtolower($sort_direction), array('asc', 'desc'))) {
            $sort_direction = 'asc';
        }

        $sql = 'SELECT t.*,s.status_name from task t
        LEFT JOIN  status s ON (s.id=t.status_id)
         order by ' . $sort_by . ' ' . $sort_direction . ' limit ' . $start . ', ' . $limit;

        $items = $this->DB->uniSQL($sql);
        return $items;
    }

    public function GetStatusesList()
    {
        $sql = 'select * from status';

        $items = $this->DB->uniSQL($sql);
        return $items;
    }

    public function GetItemInfo($id)
    {
        $sql = 'select * from task t where t.id=' . intval($id);

        $item = $this->DB->uniSQL($sql);
        if (count($item) == 1) {
            $item = $item[0];
        } else {
            $item = false;
        }
        return $item;
    }

    public function GetItemsQty()
    {
        $items = $this->DB->uniSQL('select count(*) as cnt from task ');
        return $items[0]['cnt'];
    }

    public function AddItem($data)
    {

        $this->DB->uniAdd($data, 'task');

    }

    public function EditItem($id, $data)
    {
       // $item_info=$this->GetItemInfo($id);
        $affected_rows=$this->DB->uniUpdate($data, 'task', array('id' => $id));

        if($affected_rows!=0){
            $this->DB->uniUpdate(array('date_modify'=>date('Y-m-d H:i:s')), 'task', array('id' => $id));
        }



    }
}