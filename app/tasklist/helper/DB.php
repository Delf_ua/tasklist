<?php

namespace tasklist\helper;

use tasklist\exception;

class DB
{
    public $Host = "localhost";
    public $Database = "";
    public $User = "root";
    public $Password = "";
    /**
     * @var \mysqli
     */
    private $link;
    private $ERR;


    public function __construct($Database, $Host, $User, $Password)
    {

        $this->Database = $Database;
        $this->Host = $Host;
        $this->User = $User;
        $this->Password = $Password;

        $this->link = new \mysqli($this->Host, $this->User, $this->Password, $this->Database);


        if ($this->link->connect_error) {
            throw new exception\DBException('Connect Error (' . $this->connect_errno . ') '
                . $this->connect_error);

        }
        $this->link->set_charset('utf8');


    }


    /* public: perform a query */
    function DBQuery($Query_String)
    {
        global $logger;


        if ($Query_String == "") {

            return false;
        }


        $this->Result = $this->link->query($Query_String);


        if (!$this->Result) {

            throw new exception\DBException("Invalid SQL: " . $Query_String . "<br>\r\n" .
                ($this->link->errno) . ' ' .
                $this->link->error);

        }


        return $this->Result;
    }

    public function escape($str)
    {
        return $this->link->real_escape_string($str);
    }


    function affected_rows()
    {
        return $this->link->affected_rows;
    }


    private function next_record()
    {
        if (!$this->Result) {
            $this->halt("next_record called with no query pending.");
            return 0;
        }

        $this->Record = @$this->Result->fetch_assoc();

        $stat = is_array($this->Record);
        if (!$stat) {
            $this->Result->free();
        }
        return $stat;
    }

    // Private checking function
    // -------------------------------------
    private function checkData($postData)
    {
        if (!is_array($postData)) {
            $this->ERR = "Error while inserting!";
            return false;
        }
        return true;
    }

    /* Insert something into defined table!
     * ------------------------------------------------------------------------
     * $postData = assoc array, where the keys are the table fields names too.
     * $tableName = "table name!"
     * OUTPUT -> id of new record!
     *
     * @param $postData
     * @param $tableName
     * @return int
     * @throws exception\DBException
     */
    function uniAdd($postData, $tableName)
    {
        $this->ERR = '';
        if (!$this->checkData($postData)) {
            return 0;
        }

        if (count($postData) > 0) {

            $sql = "INSERT INTO $tableName (";
            reset($postData);
            foreach ($postData as $key => $val) {
                $sql .= $key . ", ";
            }

            $sql = substr($sql, 0, -2);
            $sql .= ") VALUES (";
            reset($postData);

            foreach ($postData as $key => $val) {
                if (is_null($val)) {
                    $sql .= "NULL, ";
                } else {
                    $sql .= "'" . $this->link->real_escape_string($val) . "', ";
                }

            }
            $sql = substr($sql, 0, -2);
            $sql .= ") ";
        } else $sql = "INSERT INTO $tableName ()VALUES()";


        $this->DBQuery($sql);
        $this->ERR = $this->link->error;
        return $this->link->insert_id;
    }


    /** Update defined(or all) field(s).
     * @param $postData
     * @param $tableName
     * @param string $idArr
     * @return false|int
     * @throws exception\DBException
     */
    function uniUpdate($postData, $tableName, $idArr = "")
    {

        if (!$this->checkData($postData)) {
            return false;
        }

        $sql = "UPDATE $tableName SET ";
        foreach ($postData as $key => $val) {
            if (is_null($val)) {
                $sql .= "$key = NULL, ";
            } else {
                $sql .= "$key = '" . $this->link->real_escape_string($val) . "', ";
            }
        }
        $sql = substr($sql, 0, -2);
        if (is_array($idArr)) {
            $idval = current($idArr);
            $idkey = key($idArr);
            unset($idArr[$idkey]);
            $sql .= " WHERE  ($idkey = '$idval') ";
            foreach ($idArr as $idkey => $idval) {
                $sql .= " and  ($idkey = '$idval') ";
            }

        }


        $this->DBQuery($sql);
        if ($this->link->error) {
            $this->ERR = $this->link->error;
        }
        if (!empty($this->link->error)) {
            $result = false;
        } else {
            $result = $this->affected_rows();
        }
        return $result;
    }
    // Delete defined(or all) field(s).
    // -------------------------------------------------
    function uniDelete($idArr = "", $tableName)
    {
        $sql = "DELETE FROM $tableName ";

        if (is_array($idArr)) {
            reset($idArr);
            $idval = current($idArr);
            $idkey = key($idArr);
            unset($idArr[$idkey]);
            $sql .= " WHERE ($idkey = '$idval') ";
            foreach ($idArr as $idkey => $idval) {
                $sql .= " and ($idkey = '$idval') ";
            }
        } else {
            $sql .= "WHERE " . $idArr;
        }

        $this->DBQuery($sql);
        if ($this->error) {
            $this->ERR = $this->error;
        }
        return $this->affected_rows();
    }

    /**
     * @return array
     */
    function ResultToArray($idField)
    {
        if (!($this->Result === true)) {
            while ($this->next_record()) {

                if (!empty($idField))
                    $arr[$this->Record[$idField]] = $this->Record;
                else {
                    $arr[] = $this->Record;
                }
            }
            return isset($arr) ? (array )$arr : array();


        } else {

            return array();

        }
    }

    function uniSQL($sql, $idField = '')
    {


        $this->DBQuery($sql);

        return $this->ResultToArray($idField);

    }

    public function BeginTransaction($flags = 0, $name = null)
    {
        $this->link->begin_transaction($flags, $name);
    }

    public function CommitTransaction()
    {
        $this->link->commit();
    }

    public function RollbackTransaction()
    {
        $this->link->rollback();
    }


}


?>