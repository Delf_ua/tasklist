<?php

namespace tasklist\helper;

use tasklist\exception\GeneralException as GeneralException;

class View
{

    private $_template;
    private $_var = array();

    public function __construct($template)
    {
        $this->_template = _TPL_PATH . $template;
    }

    public function set($name, $value)
    {
        $this->_var[$name] = $value;
    }

    public function __get($name)
    {
        if (isset($this->_var[$name])) return $this->_var[$name];
        return '';
    }

    public function display($strip = false)
    {
        echo $this->fetch($strip);

    }

    public function fetch($strip = false)
    {

        if (!file_exists($this->_template)) {
            throw new GeneralException('Template ' . $this->_template . ' not exist!');
        }

        ob_start();
        include($this->_template);
        $output = ob_get_clean();
        if ($strip) {
            $output = $this->_strip($output);
        }
        return $output;
    }

    private function _strip($data)
    {
        $lit = array("\\t", "\\n", "\\n\\r", "\\r\\n", "  ");
        $sp = array('', '', '', '', '');
        return str_replace($lit, $sp, $data);
    }


}

?>