<?php

namespace tasklist\controller;

use tasklist\helper\View;
use tasklist\model\User as UserModel;


class User extends _Base
{
    /**
     * @var UserModel $UserModel
     */
    private $TaskModel;

    public function __construct()
    {
        parent::__construct();
        $this->UserModel = new UserModel();

    }


    public function Action_GetLoginForm()
    {
        $json = array();
        $view = new View('login_form.phtml');
        $json['form_html'] = $view->fetch(true);
        return $json;
    }

    public function Action_Logout()
    {
        unset($_SESSION['logged_user']);
        $json = array();
        return $json;
    }

    public function Action_Login()
    {
        parse_str($_REQUEST['form_data'], $form_data);
        $json = array();
        $json['success'] = false;
        $login = $form_data['user_login'];
        $pass = $form_data['user_pass'];
        if ($logged_user = $this->UserModel->Login($login, $pass)) {
            $json['success'] = true;
            $_SESSION['logged_user'] = $logged_user;
        }
        return $json;
    }


}

