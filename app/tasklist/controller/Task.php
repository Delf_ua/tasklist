<?php

namespace tasklist\controller;

use tasklist\helper\View;
use tasklist\model\Task as TaskModel;
use JasonGrimes\Paginator;

class Task extends _Base
{
    /**
     * @var TaskModel $TaskModel
     */
    private $TaskModel;

    public function __construct()
    {
        parent::__construct();
        $this->TaskModel = new TaskModel();

    }

    public function Action_DisplayList()
    {
        $view = new View('index.phtml');
        $sort = '';
        if (!empty($_REQUEST['sort'])) {
            $sort = $_REQUEST['sort'];
        }
        $sort_direction = 'asc';
        if (!empty($_REQUEST['sort_direction']) and in_array(strtolower($_REQUEST['sort_direction']), array('asc', 'desc'))) {
            $sort_direction = $_REQUEST['sort_direction'];
        }
        $currentPage = 1;
        if (!empty($_REQUEST['page']) and $_REQUEST['page'] > 1) {
            $currentPage = intval($_REQUEST['page']);
        }
        $totalItems = $this->TaskModel->GetItemsQty();
        $itemsPerPage = 3;

        $view->set('sort', $sort);
        $view->set('sort_direction', $sort_direction);
        $urlPattern = APP_ROOT . '?sort=' . $sort . '&sort_direction=' . $sort_direction . '&page=(:num)';

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        $view->set('paginator', $paginator);
        $logged_user = false;
        if (isset($_SESSION['logged_user'])) {
            $logged_user = $_SESSION['logged_user'];
        }

        $view->set('logged_user', $logged_user);
        $items = $this->TaskModel->GetItems($itemsPerPage * ($currentPage - 1), $itemsPerPage, $sort, $sort_direction);

        $view->set('TaskItems', $items);
        return $view->fetch();
    }

    public function Action_GetForm()
    {
        $json = array();
        $view = new View('task_form.phtml');
        $task_id = 0;
        $task_info = false;

        if (isset($_REQUEST['task_id'])) {
            $task_id = intval($_REQUEST['task_id']);
            $task_info = $this->TaskModel->GetItemInfo($task_id);
        }
        $view->set('task_info', $task_info);
        $view->set('statuses_list', $this->TaskModel->GetStatusesList());

        $json['form_html'] = $view->fetch(true);
        return $json;
    }

    public function Action_SaveForm()
    {
        parse_str($_POST['form_data'], $form_data);

        $json = array();
        $json['error_msg'] = '';
        $task_data = array('user_name' => htmlspecialchars($form_data['user_name']), 'email' => $form_data['email'],
            'task_text' => htmlspecialchars($form_data['task_text']), 'status_id' => intval($form_data['status_id']));
        $json['error_msg'] = $this->ValidateForm($task_data);
        if (empty($json['error_msg'])) {
            if (intval($form_data['id']) > 0) {
                if (empty($_SESSION['logged_user'])) {
                    $json['error_msg'] = 'You are not logged in';
                } else {
                    $this->TaskModel->EditItem(intval($form_data['id']), $task_data);
                }
            } else {
                $this->TaskModel->AddItem($task_data);
            }
        }
        return $json;

    }

    public function ValidateForm($data)
    {
        $errors = '';
        if (empty($data['user_name'])) {
            $errors .= '<br>' . 'Name can not be empty';
        }
        if (empty($data['task_text'])) {
            $errors .= '<br>' . 'Description can not be empty';
        }
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $errors .= '<br>' . 'Email is not valid';
        }
        return $errors;
    }
}

