<?php
ini_set('error_reporting', E_ALL);
ini_set('log_errors ', 1);
if(DEBUG == true)
{
    ini_set('display_errors',1);

}
else
{
    ini_set('display_errors',0);
}
session_start();
require_once __DIR__ . '/../vendor/autoload.php';
tasklist\helper\Registry::set('DB', new tasklist\helper\DB(DB_DATABASE, DB_HOST, DB_USER, DB_PASS));